#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h> 
#include <limits.h>
#include <sys/stat.h>
#include <errno.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void list_dir (const char *dir_name, int client){
  
    DIR *d = opendir(dir_name);
    if (!d) {
        switch (errno){
            case ENOTDIR: printf("%s\n",dir_name); 
            write(client, dir_name, strlen(dir_name));
            char buffer[255];
            bzero(buffer, 255);
            ssize_t n = read(client, buffer, 255);
            return;
            default: return;
        }
    }
    
    struct dirent *entry; // entry
    const char *d_name;   // entry name
    
    // while no error or no more entry
    while( (entry = readdir(d)) != NULL ) {
        d_name = entry->d_name; 
        // ignore when  '.' ou '..' #infinite recursion
        
        if (entry->d_name[0] == '.' && (entry->d_name[1] == '\0' || (entry->d_name[1] == '.' && entry->d_name[2] == 0)))
            continue;

        printf("%s\n", entry->d_name);
        // path of file/dir
        char path[PATH_MAX];
        int path_length = snprintf(path, PATH_MAX, "%s/%s", dir_name, d_name);
            
        if (entry->d_type & DT_DIR){
            if ((path_length) >= PATH_MAX) {                                            
                fprintf(stderr, "Path length has got too long : %i\n", (path_length));  
                continue;
            }
            list_dir(path, client); // recursivity if entry is a dir
        } else {
                write(client, d_name, strlen(d_name));
                char reponse[2];
                bzero(reponse, 2);
                read(client, reponse, 2);
                if (strncmp(reponse, "ok", 2) != 0) {
                    fprintf(stderr, "error, no ok\n");
                    return;
                }
        }
    }
    // close dir
    if (d && closedir(d)){                                                  
        fprintf(stderr, "Could not close '%s': %s\n", dir_name, strerror (errno)); 
    }
}


int main(int argc, char *argv[])
{
    
    if(argc < 2)
    {
        fprintf(stderr ,"Port No not provided by user. Program terminated\n");
        exit(1);
    }
    printf("IP adress of this Server is 127.0.0.2\n");
    printf("Port : %s\n", argv[1]);
    chdir("files");
    int sockfd , newsockfd , portno , n;
    char buffer[255];

    struct sockaddr_in serv_addr , cli_addr;
    socklen_t clilen;
    
    sockfd = socket(AF_INET , SOCK_STREAM , 0);
    if(sockfd < 0)
    {
        error("Error opening Socket.");
    }
    bzero((char *) &serv_addr , sizeof(serv_addr));
    portno = atoi(argv[1]);

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    if(bind(sockfd , (struct  sockaddr *) &serv_addr , sizeof(serv_addr)) < 0)
        error("Binding Failed.");
    
    //second parameter, "5" for exemple 
    // maximum limit of clients who can connact to the server at the same time 
    listen(sockfd , 5);
    clilen = sizeof(cli_addr);

    //Accept func : 1 param > file descripter
    // 2 > Typecasting to sockaddr 
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

    //Checking if func newsoxkfd is succesful or failed
    if(newsockfd < 0)
        perror ("Error on Accept");
    else 
        printf("Got connection from host %s\n", inet_ntoa(cli_addr.sin_addr));
    while (1)
    {
        bzero(buffer , 255);
        n = read(newsockfd , buffer , 255);
        if(n < 0)
            perror("Error on reading.");
        printf("Client : %s\n" , buffer);

        //trying to repare chat 
        //printf("|%s|\n", buffer);

        //n = write(newsockfd , buffer , strlen(buffer));
        if(n < 0)
            error("Error on Writing.");
        
        //Exiting 
        int i = strncmp("Bye" , buffer , 3);
        if (i == 0){
            printf("We have to say GOODBYE\n");
            break;
        }
        int l = strncmp("List" , buffer , 4);
        if (l == 0){
            //printf("Got here\n");
            list_dir(".", newsockfd);
            write(newsockfd, " All files are dispayed!", 24);

        } else if (strncmp("Get" , buffer , 3) == 0){
            //buffer+4;
            FILE *fp;
            int ch = 0;
            fp = fopen("glad_recived.txt", "a");
            int words;

            read(newsockfd, &words, sizeof(int));
            while(ch != words)
            {
                read(newsockfd, buffer, 255);
                fprintf(fp,"%s ", buffer);
                ch++;
            }
            printf("The file has been recived.");
        }
    }

    close(newsockfd);
    close(sockfd);
    return 0;


}

